CONCEPTION NIVEAU 4 : TOXIC
===

> RStokes

Largeur : 960px
Hauteur : 640px
Graduation 1/4 : 240x160

5 étages
- 128px par étage
  - Must include room for ground, room for character, room for obstacles, room for jumping
  - Faut laisser l'espace pour le sol, pour sauter, pour le personnage, pour les obstacles
  - Éviter la claustrophobie
- Étage non défini strictement (peut avoir les démi-étages, etc)

## DIFFICULTÉ 4/6 !! 

Obstacles & objets :
- [A] Barils de déchets radioactif pour pousser et utiliser comme platforme
- [B] Flaques d'eau toxique, peut pas toucher
- [C] Nuages de gaz toxique qui flottes en continu du gauche à droit, tue le personnage - faut les éviter en restant dans l'espace entre deux nuages
- [D] Platformes qui tombent si tu restes longtemps dessus (tombe après ~1.5-2 secondes)
- [E] Gouttes d'eau toxique qui tombe du plafond de façon régulière, tue le personnage


Aesthestics:
- sharp colours with soft shapes
  - black and purple
- platforms dripping with goo in them
- Glowing green goo ?

L'ésthétique
- Des couleurs vives avec des formes douces (eau, nuages, matérial collant, etc)
  - Noir, gris, violet, etc
- Goo noir = tue le personnage, goo vert = décoratif pour les barils
- Platformes avec plusiers violets différent
  - Palette de couleurs généré [ici](https://coolors.co/292929-474747-462c8c-410066-2c0735)
  - Example de platform simple avec en Paint ![ici](/toxic_sample_platform.png "Platforme toxique")