extends KinematicBody2D

const speed = 500
const jump_speed = -1300
const gravity = 4000
export (float, 0, 1.0) var friction = 0.5
export (float, 0, 1.0) var acceleration = 0.25
var AntiGravity = false
var velocity = Vector2.ZERO

func get_input():
	var dir = 0
	if Input.is_action_pressed("ui_right"):
		dir += 1
		$Sprite.play("Walk")
		$Sprite.flip_h = false
	elif Input.is_action_pressed("ui_left"):
		dir -= 1
		$Sprite.play("Walk")
		$Sprite.flip_h = true
	if dir != 0:
		velocity.x = lerp(velocity.x, dir * speed, acceleration)
	else:
		velocity.x = lerp(velocity.x, 0, friction)
		$Sprite.play("Idle")
	if not is_on_floor():
		$Sprite.play("Jump")
	
func _physics_process(delta):
	get_input()
	if AntiGravity == false:
		velocity.y += gravity * delta
	if AntiGravity == true:
		velocity.y -= gravity * delta	
	var snap = Vector2.DOWN *16 if is_on_floor() else Vector2.ZERO
	velocity = move_and_slide_with_snap(velocity, snap, Vector2.UP)
#	velocity = move_and_slide(velocity, Vector2.UP)
	if Input.is_action_just_pressed("ui_up"):
		$Sprite.play("Jump")
		if is_on_floor():
			velocity.y = jump_speed

