extends Area2D
var gear = false


func _on_MoonDoor_body_entered(body):
	print("Charlie entered Moon door")
	print("Does charlie have gear? ",gear)
	if gear == true:
		get_tree().change_scene("res://Menus/MoonMenus/MenuInterLuneLave.tscn")


func _on_LavaDoor_body_entered(body):
	print("Charlie entered Lava door")
	print("Does charlie have gear? ",gear)
	if gear == true:
		get_tree().change_scene("res://Menus/LavaMenus/LavaMenuInter.tscn")



func _on_IceDoor_body_entered(body):
	print("Charlie entered Ice door")
	print("Does charlie have gear? ",gear)
	if gear == true:
		get_tree().change_scene("res://Menus/IceMenus/IceMenuInter.tscn")


func _on_TropicalDoor_body_entered(body):
	print("Charlie entered Tropical door")
	print("Does charlie have gear? ",gear)
	if gear == true:
		get_tree().change_scene("res://Menus/TropicalMenus/TropicalMenuInter.tscn")


func _on_ToxicDoor_body_entered(body):
	print("Charlie entered Toxic door")
	print("Does charlie have gear? ",gear)
	if gear == true:
		get_tree().change_scene("res://Menus/ToxicMenus/ToxicMenuInter.tscn")

func _on_Gear_body_entered(body):
	gear = true
	print("Door: Charlie has touched the gear")

	
