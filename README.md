# Univ LR L2 Informatique

> Groupe 31, EC Gestion de projet

## Mise en place du début

### _Étapes a suivre un seul fois_

- Télécharger et installer git [ici](https://git-scm.com/downloads).
- Copier lien du dépôt git
  - Avec SSH (plus sécurisé, besoin d'un clé SSH) : git@gitlab.univ-lr.fr:simple-horse-studios/galax6.git
  - Avec HTTPS (pas de config supplémentaire) : https://gitlab.univ-lr.fr/simple-horse-studios/galax6.git
- Dans le console, naviguer jusqu'à où tu veut mettre le projet
- `$ git clone [lien-dépôt]`
- `$ cd galax6`

## Début de chaque semaine

- Naviguer vers la branche principale
  - `$ git checkout main`
- Mettre à jour ton dépôt locale avec le travail de la semaine passée
  - `$ git pull`
- Créer une branche de fonctionnalité pour le travail de la semaine en cours
  - `$ git checkout -b semaine-initiales-tâche`
  - Par example, si tâche de Ríona pour semaine 12 est la création des tilesets : `12-rs-tilesets`
- Synchroniser la création de la nouvelle branche avec le dépôt en ligne
  - `$ git push -u origin [nom-branche]`
- Vérfier que t'es sur la nouvelle branche qui vient d'être créé
  `$ git status`

## Pendant la semaine

- Enregistrer tes modifications vers ta branche de fonc. avec un commit
  - `$ git add .`
  - `$ git commit -m "Ici détails du modification"`
  - `$ git push`

## Fin de chaque semaine

- Vérfier que tout les commits sont poussé vers le remote
  - Verification avec `$ git status`
- Créer un demande de fusionnement [d'ici](https://gitlab.univ-lr.fr/simple-horse-studios/galax6/-/merge_requests/new)
  - Source branch : ton branche de fonc. pour la semaine
  - Target branch : `main`

## Planning

> Limite soft semaine 19, limite dur semaine 22

| Semaine     | Tâche A                                             | Tâche B                                                   | Tâche C                                     | Tâche D                                                 |
| ----------- | --------------------------------------------------- | --------------------------------------------------------- | ------------------------------------------- | ------------------------------------------------------- |
| **_Sem18_** | TBD                                                 | TBD                                                       | TBD                                         | TBD                                                     |
| **_Sem17_** | Niveaux dynamique fini (pour les niveaux preparées) | Niveaux dynamique fini (pour les niveaux preparées)       | Transitions entre niveaux                   | Menu fonctionnel                                        |
| **_Sem16_** | Code des obstacles x?                               | Code des obstacles x?                                     | Niveaux dynamique fini (pour les preparées) | Niveaux dynamique fini (pour les preparées)             |
| **_Sem15_** | Code du HUD, portail, débris de fusée               | Code des platformes bénignes (statiques, dynamiques, etc) | Code des obstacles x?                       | Code des obstacles x?                                   |
| **_Sem14_** | Niveau statique fini x3 niveaux                     | Niveau statique fini x3 niveaux                           | Code du joueur (marcher sauter pousser)     | Création du HUD, conception du portail, débris de fusée |
| **_Sem13_** | Tâche sem12 | Tâche sem12 | Tâche sem12 | Tâche sem12 |
| **_Sem12_** | Conception du joueur (animation statique incl)      | Création des tuiles x2 niveaux                            | Création des tuiles x2 niveaux              | Création des tuiles x2 niveaux                          |
| **Sem11**   | Conception niveau 2 (glace)                         | Conception niveau 3 (tropical)                            | Conception niveau 4 (toxic)                 | Conception niveau 5 (lave)                              |
