extends Control



func _on_StartButton_pressed():
	print("Game start, to Moon")
	get_tree().change_scene("res://NiveauLune.tscn")


func _on_QuitButton_pressed():
	print("Game quit")
	get_tree().quit()


func _on_CreditButton_pressed():
	print("To credits")
	get_tree().change_scene("res://Menus/MenuCredit.tscn")
