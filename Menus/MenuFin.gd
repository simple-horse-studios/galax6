extends Control



func _on_Menu_pressed():
	print("Menu button on end page pressed")
	get_tree().change_scene("res://Menus/Menu.tscn")

func _on_CreditButton_pressed():
	print("Credit button on end page pressed")
	get_tree().change_scene("res://Menus/MenuCredit.tscn")
	
func _on_QuitButton_pressed():
	print("Quit button on end page pressed")
	get_tree().quit()
